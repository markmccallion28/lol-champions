package com.springexercise.springlol.service;

import com.springexercise.springlol.entities.LolChampion;
import com.springexercise.springlol.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LolChampionService {

    @Autowired
    LolChampionRepository championRepository;

    public List<LolChampion> findAll(){
        return championRepository.findAll();
    }

    public LolChampion findById(long id){
        return championRepository.findById(id).get();
    }

    public LolChampion save(LolChampion champion){
        return championRepository.save(champion);
    }
}
