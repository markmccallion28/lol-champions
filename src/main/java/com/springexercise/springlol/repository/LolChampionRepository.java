package com.springexercise.springlol.repository;

import com.springexercise.springlol.entities.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LolChampionRepository extends JpaRepository<LolChampion, Long> {
}

