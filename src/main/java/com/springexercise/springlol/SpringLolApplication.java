package com.springexercise.springlol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLolApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLolApplication.class, args);
    }

}
