package com.springexercise.springlol.controller;

import com.springexercise.springlol.entities.LolChampion;
import com.springexercise.springlol.service.LolChampionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/v1/lolchampion")
public class LolChampionController {
    @Autowired
    private LolChampionService championService;

    @GetMapping
    public List<LolChampion> findAll(){
        return championService.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<LolChampion> findById(@PathVariable long id) {
        try {
            return new ResponseEntity<LolChampion>(championService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<LolChampion> create(@RequestBody LolChampion lolChampion){
        try {
            return new ResponseEntity<LolChampion>(championService.save(lolChampion), HttpStatus.CREATED);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
